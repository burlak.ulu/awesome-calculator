package com.company;

import java.util.ArrayList;

public class Fraction {
    private int numerator;
    private int denominator;

    Fraction(int num, int denom) {
        numerator = num;
        denominator = denom;
        makeCorrect();
    }

    Fraction(String f) {
        int slashIndex = f.indexOf('/');
        if (slashIndex == -1) {
            numerator = Integer.parseInt(f);
            denominator = 1;
        } else {
            numerator = Integer.parseInt(f.substring(0, slashIndex));
            denominator = Integer.parseInt(f.substring(slashIndex + 1));
        }
    }

    public void makeCorrect() {
        if (denominator == 0) {
            System.out.println("wrong fraction");
            System.exit(0);
        }
        for (int i = denominator; i > 1; i--) {
            if ((numerator % i == 0) && (denominator % i == 0)) {
                numerator = numerator / i;
                denominator = denominator / i;
            }
        }
        if (denominator < 0 && numerator < 0) {
            numerator = Math.abs(numerator);
            denominator = Math.abs(denominator);
        }
        if (numerator < 0 || denominator < 0) {
            numerator = (-1) * Math.abs(numerator);
            denominator = Math.abs(denominator);
        }
    }

    public void print() {
        System.out.println(numerator + "/" + denominator);
    }
    public String printFraction () {
        return (numerator + "/" + denominator);
    }
    public static void printFractions(ArrayList<Fraction> numbers) {
        for (int i = 0; i < numbers.size(); i++) {
            Fraction f = numbers.get(i);
            f.print();
        }
    }
    public Fraction add(Fraction f) {
        numerator = numerator * f.denominator + f.numerator * denominator;
        denominator = denominator * f.denominator;
        Fraction result = new Fraction(numerator, denominator);
        result.makeCorrect();
        //result.print();
        return result;
    }
    public Fraction sub(Fraction f) {
        numerator = numerator * f.denominator - f.numerator * denominator;
        denominator = denominator * f.denominator;
        Fraction result = new Fraction(numerator, denominator);
        result.makeCorrect();
        //result.print();
        return result;
    }
    public Fraction mult(Fraction f) {
        numerator = numerator * f.numerator;
        denominator = denominator * f.denominator;
        Fraction result = new Fraction(numerator, denominator);
        result.makeCorrect();
        //result.print();
        return result;
    }
    public Fraction div(Fraction f) {
        numerator = numerator * f.denominator;
        denominator = denominator * f.numerator;
        Fraction result = new Fraction(numerator, denominator);
        result.makeCorrect();
        //result.print();
        return result;
    }
}

