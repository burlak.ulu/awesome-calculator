package com.company;

import java.util.ArrayList;
import java.util.Stack;

public class PolishNot {
    private String stat;
    private ArrayList<String> expression;
    private ArrayList<String> exit;
    public PolishNot(String statement) {
        stat = statement;
        expression = express();
        exit = polishNot();
    }
    public ArrayList<String> express() {
        String[] elements = stat.split("");
        String numbers = "[0-9]+";
        String frac = "[/]+";
        String mult = "[*]+";
        String add = "[+]+";
        String sub = "[-]+";
        String div = "[:]+";
        String scob1 = "[(]+";
        String scob2 = "[)]+";
        ArrayList<String> expression = new ArrayList<>();
        String temp = "";
        for (int i = 0; i < elements.length; i++) {
            if (elements[i].matches(numbers) || elements[i].matches(frac))
                temp += elements[i];
            else if (elements[i].matches(scob1))
                expression.add(elements[i]);
            else if (elements[i-1].matches(scob2))
                expression.add(elements[i]);
            else {
                expression.add(temp);
                expression.add(elements[i]);
                temp = "";
            }
            if (i == elements.length - 1 && !(elements[i].matches(scob2)))
                expression.add(temp);
        }
        System.out.println("expression" + expression);
        ArrayList<String> fractions = new ArrayList<>();
        for (int i = 0; i < expression.size(); i++) {
            if (!(expression.get(i).matches(scob2)) && !(expression.get(i).matches(scob1)) && !(expression.get(i).matches(add)) && !(expression.get(i).matches(mult)) && !(expression.get(i).matches(sub)) && !(expression.get(i).matches(div))) {
                Fraction fraction = new Fraction(expression.get(i));
                fraction.makeCorrect();
                fractions.add(fraction.printFraction());
            } else fractions.add(expression.get(i));
        }
        System.out.println("fractions" + fractions);
        return(fractions);
    }
    public ArrayList<String> polishNot() {
        ArrayList<String> exit = new ArrayList<>();
        String add = "[+]+";
        String sub = "[-]+";
        String mult = "[*]+";
        String div = "[:]+";
        String scob1 = "[(]+";
        String scob2 = "[)]+";
        Stack<String> signs = new Stack<>();
        for (int i = 0; i < expression.size(); i++) {
            if (!(expression.get(i).matches(scob1)) && !(expression.get(i).matches(scob2)) && !(expression.get(i).matches(add)) && !(expression.get(i).matches(sub)) && !(expression.get(i).matches(mult)) && !(expression.get(i).matches(div)))
                exit.add(expression.get(i));
            else if (expression.get(i).matches(scob1))
                signs.push(expression.get(i));
            else if (expression.get(i).matches(scob2)) {
                while (!(signs.empty())) {
                    String a = signs.pop();
                    if (a.matches(scob1))
                        break;
                    exit.add(a);
                }
                if (!(signs.empty())) {
                    String b = signs.pop();
                    if (b.matches(add) || b.matches(sub) || b.matches(mult) || (b.matches(div)))
                        exit.add(b);
                    else signs.push(b);
                }
            }
            else if (expression.get(i).matches(add) || (expression.get(i).matches(sub) || expression.get(i).matches(mult) || (expression.get(i).matches(div)))) {
                if (i == expression.size() - 1) {
                    System.out.println("некорректно задано выражение");
                    break;
                }
                else if (signs.empty() || priot(signs.peek()) < priot(expression.get(i)) || signs.peek().matches(scob1))
                    signs.push(expression.get(i));
                else if (!(priot(signs.peek()) < priot(expression.get(i)))) {
                    exit.add(signs.pop());
                    signs.push(expression.get(i));
                }
            }
        }
        while (!(signs.empty()))
            exit.add(signs.pop());
        System.out.println("exit"+exit);
        return(exit);
    }
    public String meaning() {
        Stack<String> stack = new Stack<>();
        String mult = "[*]";
        String add = "[+]";
        String sub = "[-]";
        String div = "[:]";
        for (int k = 0; k < exit.size(); k++) {
            if (!(exit.get(k).matches(mult)) && !(exit.get(k).matches(add)) && !(exit.get(k).matches(sub)) && !(exit.get(k).matches(div)))
                stack.push(exit.get(k));
            else {
                String temp1 = stack.pop();
                Fraction tempA = new Fraction(temp1);
                String temp2 = stack.pop();
                Fraction tempB = new Fraction(temp2);
                if (exit.get(k).matches(add)) {
                    tempA = tempA.add(tempB);
                    stack.push(tempA.printFraction());
                } else if (exit.get(k).matches(mult)) {
                    tempA = tempA.mult(tempB);
                    stack.push(tempA.printFraction());
                } else if (exit.get(k).matches(sub)) {
                    tempA = tempB.sub(tempA);
                    stack.push(tempA.printFraction());
                } else if (exit.get(k).matches(div)) {
                    tempA = tempB.div(tempA);
                    stack.push(tempA.printFraction());
                }
            }
        }
        String res = stack.pop();
        return(res);
    }
    public static int priot(String a) {
        String sign1 = "[*]";
        String sign2 = "[:]";
        int priot = 1;
        if (a.matches(sign1) || a.matches(sign2)) priot = 2;
        return priot;
    }
}
