import os
project_root = os.getcwd()
for root, dirs, files in os.walk(project_root):
    for file in files:
        if file.endswith(".java"):
            with open(os.path.join(root,file),"r") as newFile:
                content = newFile.readlines()
                if not content[0].startswith('// Copyright'):
                    print(f"COPYRIGHT IS NOT FOUND IN THIS PART: {os.path.basename(file)}")
                else:
                    print(f"COPYRIGHT IS FOUND IN THIS PART: {os.path.basename(file)}")