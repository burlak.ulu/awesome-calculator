package com.company;

import org.junit.Test;

import static org.junit.Assert.*;

public class PolishNotTest {

    @Test
    public void meaning() {
        PolishNot stat = new PolishNot("(15/5+5)*2");
        assertEquals("16/1", stat.meaning());
    }
    @Test
    public void meaning1() {
        PolishNot stat1 = new PolishNot("(15-9):(2/2+35/7)");
        assertEquals("1/1", stat1.meaning());
    }
}